deepArch
========

This project is created purely for my educational purpose of learning deep architectures, check out what deep architectures are capable of: http://youtu.be/ZmNOAtZIgIk.

Documentation
=============

Please go to http://hencrice.bitbucket.org/deepArch/.

Data sets/optimized architectural parameters
============================================

Please refer to the Sourceforge page of this project at: http://sourceforge.net/projects/deeparch/files/